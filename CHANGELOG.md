# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- TOC -->
* [Changelog](#changelog)
  * [[Unreleased]](#unreleased)
  * [[0.0.1] - 2024-02-06](#001---2024-02-06)
    * [Added](#added)
<!-- TOC -->

## [Unreleased]

## [0.0.1] - 2024-02-06
### Added
- [#1 Initial project setup](https://gitlab.com/projet-s10-console-finops-multicloud/finops-one-api/-/merge_requests/1):
  - Added README.md
  - Added .gitignore
  - Added LICENSE
  - Added CHANGELOG.md
- [#2 Add CI/CD](https://gitlab.com/projet-s10-console-finops-multicloud/finops-one-api/-/merge_requests/2)
- [#3 Setup quality code control](https://gitlab.com/projet-s10-console-finops-multicloud/finops-one-api/-/merge_requests/3)

